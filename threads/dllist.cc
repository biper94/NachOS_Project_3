#include "dllist.h"
#include <iostream>
#include <assert.h>
using namespace std;

DLLElement::DLLElement(void *itemPtr, int sortKey)
	: item(itemPtr)
	, key(sortKey)
	, next(NULL)
	, prev(NULL) {};

DLList::DLList()
	: first(NULL)
	, last(NULL) {};

bool DLList::IsEmpty() {
	return (first == NULL) && (last == NULL);
}

void DLList::Prepend(void* itemPtr) {

	if (IsEmpty()) {
		DLLElement* element = new DLLElement(itemPtr, 0);
		first 	= element;
		last 	= element;
	}
	else {
		DLLElement* element = new DLLElement(itemPtr, first->key - 1);
		element->next 	= first;
		first->prev 	= element;
		first 			= element;
	}

}

void DLList::Append(void* itemPtr) {
	if (IsEmpty()) {
		DLLElement* element = new DLLElement(itemPtr, 0);
		first = element;
		last = element;
	}
	else {
		DLLElement* element = new DLLElement(itemPtr, last->key + 1);
		last->next 	= element;

		last = element;
	}
}

void* DLList::remove(int *keyPtr) {
	// rmove from head of list
	// set *keyPtr to key of the removed item
	// return item (or NULL if list is empty)

	if(IsEmpty()) {
		return NULL;
	}
	else {
		DLLElement* prevFirst = first;
		DLLElement* newFirst = first->next;
		if(newFirst != NULL)
			newFirst->prev = NULL;
		else {
			last = newFirst;
		}
		*keyPtr = first->key;

		first = newFirst;


		return prevFirst->item;

	}
}
// routines to put/get items on/off list n order (sorted by key)
void DLList::SortedInsert(void *item, int sortKey) {

	DLLElement* elePtr = first;
	DLLElement* newElement = new DLLElement(item, sortKey);
	if (IsEmpty()) {

		first = newElement;
		last = newElement;
	}
	else if(sortKey <= first->key) {

			newElement->next = first;
			first->prev 	= newElement;
			first 			= newElement;

		}
	else if (sortKey >= last->key) {
		newElement->prev = last;
		last->next 	= newElement;

		last = newElement;
	}
	else {
		while(elePtr != NULL) {

			if (sortKey >= elePtr->key) {
				if (elePtr->next == NULL) {
					newElement->next = elePtr;
					elePtr->prev = newElement;
					break;
				}
				else {
					if(sortKey < elePtr->next->key){
						newElement->next = elePtr->next;
						newElement->prev = elePtr;
						elePtr->next->prev = newElement;
						elePtr->next = newElement;
						break;
					}
				}
			}
			elePtr = elePtr->next;
		}
	}


}
void* DLList::SortedRemove(int sortKey) {
	// remove sorted item with (sortkey == key)
	// return null if no such item exists
	DLLElement* elePtr = first;

	while(elePtr != NULL) {
		if(sortKey == elePtr->key){
			if(elePtr == first)
				first = elePtr->next;
			else if (elePtr == last)
				last = elePtr->prev;
			else {
				elePtr->prev->next = elePtr->next;
				elePtr->next->prev = elePtr->prev;
			}

			return elePtr->item;
		}
		elePtr = elePtr->next;
	}

	cout << "sortKey " << sortKey << " does not exists!" << endl;
	return NULL;
}

DLList::~DLList() {
	if (!IsEmpty()) {
		cout << "------------------------------------------------------------------" << endl;
		RecursiveDestuctor(first);
		cout << "------------------------------------------------------------------" << endl;
	}

}
void DLList::RecursiveDestuctor(DLLElement* elePtr) {
	if(!IsEmpty() && elePtr != NULL) {
		if (elePtr->next != NULL)
			RecursiveDestuctor(elePtr->next);
		cout << "\t\tDeleting key no." << elePtr->key << endl;
		delete elePtr->item;
		delete elePtr;
	}
}

void DLList::Print_char_item() {
	DLLElement* elePtr = first;
	if (IsEmpty()) {
		cout << "Empty List.." << endl;
	}
	else {
		cout << "------------------------------------------------------------------" << endl;
		cout << "\t\t[ KEY : ITEM ]\n";
		while(elePtr != NULL) {
			if (elePtr == first)
				cout << "\nFirst-> ";

			cout << "[ " << elePtr->key << " : ";
			if (elePtr->item == NULL)
				cout << " NULL" << " ]->";
			else
				cout << *(char*)elePtr->item << " ]->";

			if (elePtr == last)
				cout << "<-Last" << endl;
			elePtr = elePtr->next;
		}
		cout << "------------------------------------------------------------------" << endl;
	}

}
