#ifndef SLEEP_THREAD_H
#define SLEEP_THREAD_H

#include "thread.h"
#include <list>

class Sleep_thread {
private:
    class Sleeping {
    public:
        Thread* SThread;
        int whenToWakeUp;
        Sleeping(Thread *t, int WTW)
            : SThread(t), whenToWakeUp(WTW) {};
    };
    int _current_interrupt;
    std::list<Sleeping> sleep_thread_list;

public:
    Sleep_thread(): _current_interrupt(0) {};
    void PutToSleep(Thread* t, int x);
    bool WakeUpThreads();
    bool IsEmpty() { return sleep_thread_list.size() == 0; }
};

#endif
